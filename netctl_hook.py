#!/usr/bin/python

import os
import subprocess

WIFI_FILE = '/tmp/wifi_info.txt'

def parse_wpa_cli_output(s):
    ssid = ''
    gen = ''
    ip = ''
    lines = s.split('\n')
    for line in lines:
        line = line.strip()
        if line.find('=') != -1:
            spl = line.split('=')
            name = spl[0]
            value = spl[1]
            if name == 'ssid':
                ssid = value
            elif name == 'wifi_generation':
                gen = value
            elif name == 'ip_address':
                ip = value
    with open(WIFI_FILE, mode='w') as w_file:
        w_file.write('ip=' + str(ip))
        w_file.write('\n')
        w_file.write('gen=' + str(gen))
        w_file.write('\n')
        w_file.write('ssid=' + str(ssid))
        w_file.write('\n')

    os.chmod(WIFI_FILE, 0o644)

def on_connect(interface):
    if interface == 'wlan0':
        output = subprocess.check_output(['wpa_cli', '-i', 
                interface, 'status'], universal_newlines=True)
        parse_wpa_cli_output(output)

def on_disconnect(interface):
    if interface == 'wlan0':
        os.remove(WIFI_FILE)

if __name__=='__main__':
    interface = os.environ.get('INTERFACE')
    action = os.environ.get('ACTION')

    if action == 'CONNECTED':
        on_connect(interface)
    elif action == 'DISCONNECTED':
        on_disconnect(interface)

    # reload i3blocks
    cmd = ['/usr/bin/pkill', '-RTMIN+10', 'i3blocks']
    subprocess.check_output(cmd, universal_newlines=True)



