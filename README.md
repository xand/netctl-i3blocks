# netctl-i3blocks

![netctl-i3blocks diagram](/doc/netctl-auto.drawio.png "netctl-i3blocks diagram")

## Installation

### Link the hook

```
sudo ln -s /home/xand/ver/personal/netctl-i3blocks/wifi /etc/netctl/hooks/wifi
```

Double check that the file is executable.

### i3blocks configuration

```
[wireless]
label=  
command=/usr/bin/python /home/xand/bin/i3blocks/wifi.py
interval=once
signal=10
```


