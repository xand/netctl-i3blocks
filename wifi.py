#!/usr/bin/python

import os

WIFI_FILE = '/tmp/wifi_info.txt'

def read_file():
    if os.path.exists(WIFI_FILE):
        ip = ''
        gen = ''
        ssid = ''
        with open(WIFI_FILE, mode='r') as r_file:
            content = r_file.read()
            lines = content.split('\n')
            for line in lines:
                line = line.strip()
                if line.find('=') != -1:
                    s = line.split('=')
                    name = s[0]
                    value = s[1]
                    if name == 'ip':
                        ip = value
                    elif name == 'gen':
                        gen = value
                    elif name == 'ssid':
                        ssid = value
        return (ssid, gen, ip)

    return None

if __name__=='__main__':
    wifi = read_file()
    if wifi == None:
        os.system('echo "NOT CONNECTED"')
        os.system('echo ""')
        os.system('echo "#FF1A1A"')
        os.system('echo "#000000"')
    else:
        if wifi[1] == '4':
            os.system('echo "2.4G  ' + wifi[0] + ' (' + wifi[2] + ')"')
        else:
            os.system('echo "5G  ' + wifi[0] + '  (' + wifi[2] + ')"')

        os.system('echo ""')
        os.system('echo "#FFFFFF"')
        os.system('echo "#000000"')
    
